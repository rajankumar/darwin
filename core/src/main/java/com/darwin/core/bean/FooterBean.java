package com.darwin.core.bean;

public class FooterBean {

	public String getFooter() {
		return footer;
	}
	public void setFooter(String footer) {
		this.footer = footer;
	}
	public String getSubFooter() {
		return subFooter;
	}
	public void setSubFooter(String subFooter) {
		this.subFooter = subFooter;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getImgPath() {
		return imgPath;
	}
	public void setImgPath(String imgPath) {
		this.imgPath = imgPath;
	}
	private String footer;
	private String subFooter;
	private String url;
	private String imgPath;

}
