
package com.darwin.core.models;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.RepositoryException;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Required;
import org.apache.sling.models.annotations.Source;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.darwin.core.bean.FooterBean;

@Model(adaptables = SlingHttpServletRequest.class)
public class FooterMultifieldModel {

	@Inject
	@Source("sling-object")
	@Required
	private ResourceResolver resourceResolver;

	@Inject
	private Resource resource;

	@Self
	private SlingHttpServletRequest slingHttpServletRequest;

	private final static Logger log = LoggerFactory.getLogger(FooterMultifieldModel.class);
	private List<FooterBean> footerDataSetList = null;

	private Map<String, List<FooterBean>> dataSet = new LinkedHashMap();

	public Map<String, List<FooterBean>> getDataSet() {
		return dataSet;
	}

	@PostConstruct
	public void init() {
		Node node = resource.adaptTo(Node.class);
		try {
			if (node != null) {
				FooterBean FooterBean = null;
				if (node.hasNode("countries")) {
					Node parentNode = node.getNode("countries");
					NodeIterator headerIterator = parentNode.getNodes();
					while (headerIterator.hasNext()) {
						footerDataSetList = new ArrayList<FooterBean>();
						String footerVal = null;
						Node childNode = headerIterator.nextNode();
						FooterBean = new FooterBean();
						if (childNode.hasProperty("country")) {
							footerVal = childNode.getProperty("country").getString();
							FooterBean.setFooter(footerVal);
						}

						if (childNode.hasNode("states")) {
							Node footerNodes = childNode.getNode("states");
							NodeIterator subFooterNodesIterator = footerNodes.getNodes();
							while (subFooterNodesIterator.hasNext()) {
								Node subFooterNodes = subFooterNodesIterator.nextNode();
								if (subFooterNodes.hasProperty("path"))
									FooterBean.setUrl(subFooterNodes.getProperty("path").getString());
								if (subFooterNodes.hasProperty("state"))
									FooterBean.setSubFooter(subFooterNodes.getProperty("state").getString());
								if (subFooterNodes.hasProperty("imgPath"))
									FooterBean.setImgPath(subFooterNodes.getProperty("imgPath").getString());
								footerDataSetList.add(FooterBean);
								FooterBean = new FooterBean();
							}

						}
						dataSet.put(footerVal, footerDataSetList);
					}

				}
			}
		} catch (RepositoryException e) {
			log.error("RepositoryException inside HeaderMultifieldModel" + e);
		}

	}
}
