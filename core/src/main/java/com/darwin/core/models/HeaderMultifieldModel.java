
package com.darwin.core.models;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.RepositoryException;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Required;
import org.apache.sling.models.annotations.Source;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.darwin.core.bean.HeaderBean;

@Model(adaptables = SlingHttpServletRequest.class)
public class HeaderMultifieldModel {

	@Inject
	@Source("sling-object")
	@Required
	private ResourceResolver resourceResolver;

	@Inject
	private Resource resource;

	@Self
	private SlingHttpServletRequest slingHttpServletRequest;

	private final static Logger log = LoggerFactory.getLogger(HeaderMultifieldModel.class);
	private List<HeaderBean> headerDataSetList = null;

	private Map<String, List<HeaderBean>> dataSet = new LinkedHashMap();

	public Map<String, List<HeaderBean>> getDataSet() {
		return dataSet;
	}

	@PostConstruct
	public void init() {
		Node node = resource.adaptTo(Node.class);
		try {
			if (node != null) {
				HeaderBean headerBean = null;
				if (node.hasNode("countries")) {
					Node parentNode = node.getNode("countries");
					NodeIterator headerIterator = parentNode.getNodes();
					while (headerIterator.hasNext()) {
						headerDataSetList = new ArrayList<HeaderBean>();
						String headerVal = null;
						Node childNode = headerIterator.nextNode();
						headerBean = new HeaderBean();
						if (childNode.hasProperty("country")) {
							headerVal = childNode.getProperty("country").getString();
							headerBean.setHeader(headerVal);
						}

						if (childNode.hasNode("states")) {
							Node headerNodes = childNode.getNode("states");
							NodeIterator subHeaderNodesIterator = headerNodes.getNodes();
							while (subHeaderNodesIterator.hasNext()) {
								Node subHeaderNodes = subHeaderNodesIterator.nextNode();
								if (subHeaderNodes.hasProperty("path"))
									headerBean.setUrl(subHeaderNodes.getProperty("path").getString());
								if (subHeaderNodes.hasProperty("state"))
									headerBean.setSubHeader(subHeaderNodes.getProperty("state").getString());
								if (subHeaderNodes.hasProperty("imgPath"))
									headerBean.setImgPath(subHeaderNodes.getProperty("imgPath").getString());
								headerDataSetList.add(headerBean);
								headerBean = new HeaderBean();
							}

						}
						dataSet.put(headerVal, headerDataSetList);
					}

				}
			}
		} catch (RepositoryException e) {
			log.error("RepositoryException inside HeaderMultifieldModel" + e);
		}

	}
}
