package com.darwin.services;

public interface ListFactory {
	AbstractPageList getPageList(String viewName);
}
